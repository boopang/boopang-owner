export class LocationService{
    private location:{title:String, desc:String, imgUrl:String}[]=[];

    add(title:String, desc:String, imgUrl:String){
        this.location.push({title,desc,imgUrl})
    }
    get(){
        return this.location;
    }
}