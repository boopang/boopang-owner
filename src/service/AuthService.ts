import firebase from 'firebase';
import { Injectable } from '@angular/core';
import { WebService } from './WebService';
import { Headers } from '@angular/http';
import { DatabaseService } from './database.service';

@Injectable()
export class AuthService{
    email = "";
    id = "";

    constructor(private webSvc:WebService, private dbSvc:DatabaseService){}
    
    signup(email: string, password: string){
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }

    signin(email:string, password:string){
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    // login(email:string, password:string, onSuccess:Function){
    //     let url = "http://delthraze.esy.es/Boopang/API/sign_in.php";
    //     let requestData = {
    //         "email" : email,
    //         "password" : password,
    //         "type" : "OWNER"
    //     };
    //     let header = new Headers({
    //         'Content-Type': 'application/json'
    //     });

    //     this.webSvc.post(url, JSON.stringify(requestData), header)
    //     .subscribe(response => {
    //         if(response == null){
    //         }
    //         else{
    //             let responseData:any = JSON.parse(response["_body"]);
    //             console.log(JSON.stringify(responseData));
    //             this.id = responseData['id']
    //             let query = "INSERT INTO tbl_user(id) VALUES ('"+responseData['id']+"')"
    //             this.dbSvc.query(query, ()=>{
    //                 onSuccess();
    //             }, ()=>{})
    //         }
    //     }, error => {
    //     });
    // }

    // signup(first_name:string, last_name:string,email: string, no_hp:string, password: string, onSuccess:Function){
    //     let url = "http://delthraze.esy.es/Boopang/API/sign_up.php";
    //     let requestData = {
    //         "first_name": first_name,
    //         "last_name":last_name,
    //         "email" : email,
    //         "phone_number" : no_hp,
    //         "password" : password,
    //         "type" : "OWNER"
    //     };
    //     let header = new Headers({
    //         'Content-Type': 'application/json'
    //     });

    //     this.webSvc.post(url, JSON.stringify(requestData), header)
    //     .subscribe(response => {
    //         if(response == null){
    //         }
    //         else{
    //             let responseData:any = JSON.parse(response["_body"]);
    //             console.log(JSON.stringify(responseData));
    //             this.login(email, password, onSuccess)
    //         }
    //     }, error => {
    //     });
    // }


    logout(){
        firebase.auth().signOut();
    //     let url = "http://delthraze.esy.es/Boopang/API/sign_in.php";
    //     let requestData = {
    //     };
    //     let header = new Headers({
    //         'Content-Type': 'application/json'
    //     });

    //     this.webSvc.post(url, JSON.stringify(requestData), header)
    //     .subscribe(response => {
    //         if(response == null){
    //         }
    //         else{
    //             let responseData:any = JSON.parse(response["_body"]);
    //             console.log(JSON.stringify(responseData));
    //             let query = "DELETE FROM tbl_user WHERE 1";
    //             this.dbSvc.query(query, ()=>{
    //                 // Kalo uda sukses logout ngapain
    //                 console.log("logout");
    //                 //return true;
    //             }, ()=>{})
    //         }
    //     }, error => {
    //     });
    // }
    // getActiveUser(){
    //     return firebase.auth().currentUser;
    // }
    }
    
}