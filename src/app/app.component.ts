import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { RegisterFieldFirstPage } from '../pages/register-field-first/register-field-first';
import { TabsPage } from '../pages/tabs/tabs';
import firebase from 'firebase';
import { DatabaseService } from '../service/database.service';
import { AuthService } from '../service/AuthService';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  isLogin = false;

  @ViewChild('sideMenuContent') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private dbSvc:DatabaseService, private authService: AuthService) {
    firebase.initializeApp({
      apiKey: "AIzaSyDhUGATGsm3QzNKJgkQKsa0PynhOof_i64",
      authDomain: "boopang-owner.firebaseapp.com"
    })
    firebase.auth().onAuthStateChanged(user =>{
      if(user){
        this.isLogin = true;
        this.nav.setRoot(TabsPage);
        // this.authService.logout();
      }
      else{
        this.isLogin = false;
        this.nav.setRoot(TabsPage);
      }
    })
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      //this.dbSvc.initDatabase();

    //   let query = "SELECT id FROM tbl_user";
    //   console.log(query);
    //   this.dbSvc.query(query, (data:any)=>{
    //     let len = data.rows.length;
    //     let asd = data.rows.item[0].id;
    //     if(len > 0){
    //       // Uda pernah login
    //       console.log(asd)
    //       this.isLogin = true;
    //       this.nav.setRoot(TabsPage);
    //     }
    //     else{
    //       // Belom pernah login
    //       this.isLogin = false;
    //       this.nav.setRoot(SignInPage);
    //     }
    //   }, ()=>{
    //     // Kalo gagal, 
    //   })
     });
  }
}

