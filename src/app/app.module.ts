import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule , ModalController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { RegisterFieldFirstPage } from '../pages/register-field-first/register-field-first';
import { RegisterFieldSecondPage } from '../pages/register-field-second/register-field-second';
import { RegisterFieldThirdPage } from '../pages/register-field-third/register-field-third';
import { TabsPage } from '../pages/tabs/tabs';
import { TabBookingPage } from '../pages/tab-booking/tab-booking';
import { TabEventPage } from '../pages/tab-event/tab-event';
import { TabFieldPage } from '../pages/tab-field/tab-field';
import { TabProfilePage } from '../pages/tab-profile/tab-profile';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { AgmCoreModule } from "@agm/core";
import { TabBookingWaitingPage } from '../pages/tab-booking-waiting/tab-booking-waiting';
import { TabBookingOnProgressPage } from '../pages/tab-booking-on-progress/tab-booking-on-progress';
import { TabBookingDonePage } from '../pages/tab-booking-done/tab-booking-done';
import { AddNewEventPage } from '../pages/add-new-event/add-new-event';
import { AuthService } from '../service/AuthService';
import { ChangePasswordPage } from '../pages/change-password/change-password';

import { NgCalendarModule  } from 'ionic2-calendar';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { WebService } from '../service/WebService';
import { WithdrawPage } from '../pages/withdraw/withdraw';
import { ManageFieldFirstPage } from '../pages/manage-field-first/manage-field-first';
import { ManageFieldSecondPage } from '../pages/manage-field-second/manage-field-second';
import { ManageFieldThirdPage } from '../pages/manage-field-third/manage-field-third';
import { HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { LocationService } from '../service/LocationService';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { DatabaseService } from '../service/database.service';
import { BoopayHistoryPage } from '../pages/boopay-history/boopay-history';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignInPage,
    SignUpPage,
    RegisterFieldFirstPage,
    RegisterFieldSecondPage,
    RegisterFieldThirdPage,
    TabsPage,
    TabBookingPage,
    TabEventPage,
    TabFieldPage,
    TabProfilePage,
    TabBookingWaitingPage,
    TabBookingOnProgressPage,
    TabBookingDonePage,
    AddNewEventPage,
    ChangePasswordPage,
    EditProfilePage,
    WithdrawPage,
    ManageFieldFirstPage,
    ManageFieldSecondPage,
    ManageFieldThirdPage,
    PrivacyPolicyPage,
    TermsOfServicePage,
    BoopayHistoryPage
    
  ],
  imports: [
    BrowserModule,
    NgCalendarModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAEgWtaInXR9XeiGEarWNtbh2dbFKEsxKU"
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignInPage,
    SignUpPage,
    RegisterFieldFirstPage,
    RegisterFieldSecondPage,
    RegisterFieldThirdPage,
    TabsPage,
    TabBookingPage,
    TabEventPage,
    TabFieldPage,
    TabProfilePage,
    TabBookingWaitingPage,
    TabBookingOnProgressPage,
    TabBookingDonePage,
    AddNewEventPage,
    ChangePasswordPage,
    EditProfilePage,
    WithdrawPage,
    ManageFieldFirstPage,
    ManageFieldSecondPage,
    ManageFieldThirdPage,
    PrivacyPolicyPage,
    TermsOfServicePage,
    BoopayHistoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    LocationService,
    WebService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    Camera,
    DatabaseService,
    SQLite
  ]
})
export class AppModule {}
