import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabFieldPage } from './tab-field';

@NgModule({
  declarations: [
    TabFieldPage,
  ],
  imports: [
    IonicPageModule.forChild(TabFieldPage),
  ],
})
export class TabFieldPageModule {}
