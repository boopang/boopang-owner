import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { RegisterFieldFirstPage } from '../register-field-first/register-field-first';
import { ManageFieldFirstPage } from '../manage-field-first/manage-field-first';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { RegisterFieldThirdPage } from '../register-field-third/register-field-third';
import { WebService } from '../../service/WebService';

/**
 * Generated class for the TabFieldPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-field',
  templateUrl: 'tab-field.html',
})
export class TabFieldPage {
  loading: Loading
  selectedProvince: any;
  selectedCity: any;
  courtCollection: any;

  updateStatus: string;

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }
  
  addNewCourt(){
    this.navCtrl.push(RegisterFieldFirstPage);
  }

  openManageFieldFirstPage(court){
    this.navCtrl.push(ManageFieldFirstPage,court);
  }

  openAlertDelete(){
    let alert = this.alertCtrl.create({
      title: 'Delete Field',
      subTitle: 'Please Contact Admin If You Want to Delete Your Court',
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });
    alert.present();
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private alertCtrl:AlertController, private webService: WebService,
    private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    //this.showLoading();
    this.getCourt();
    console.log('ionViewDidLoad TabFieldPage');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  ionViewWillEnter(){
    this.getCourt();
  }

  ngForLast(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }
  }

  isWaiting(status){
    if(status == "Waiting Approval"){
      return true
    }
    else return false
  }
  presentAlertPublish() {
    let alert = this.alertCtrl.create({
      title: 'Publish Promo',
      subTitle: 'You have just published your promo',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentAlertUnpublish() {
    let alert = this.alertCtrl.create({
      title: 'Unpublish Promo',
      subTitle: 'You have just unpublished your promo',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  changeStatus(id,status){
    console.log(id)
    console.log(status)

    if(status == "Publish"){
      this.updateStatus = "Unpublish"
      this.presentAlertUnpublish();
    }
    else {
      this.updateStatus = "Publish"
      this.presentAlertPublish();
    }
    console.log(this.updateStatus)

    let req = {
      'id_court'  : id,
      'status'    : this.updateStatus
    }

    this.webService.post("http://delthraze.esy.es/Boopang/API/update_status_court.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(responseData)
      if(responseData){
        document.getElementById('status-'+id).innerHTML = this.updateStatus
      }
    }, error =>{
    })

  }

  getCourt(){
    let req = "hai"
    // this.presentLoadingDefault();
    this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_court.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        this.courtCollection = responseData;
      }
    }, error =>{
    }) 
    
  }

}
