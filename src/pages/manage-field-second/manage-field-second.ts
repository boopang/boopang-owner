import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ManageFieldThirdPage } from '../manage-field-third/manage-field-third';

/**
 * Generated class for the ManageFieldSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manage-field-second',
  templateUrl: 'manage-field-second.html',
})
export class ManageFieldSecondPage {

  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openManageFieldThirdPage(){
    this.navCtrl.push(ManageFieldThirdPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageFieldSecondPage');
  }

}
