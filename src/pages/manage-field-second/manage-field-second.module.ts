import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageFieldSecondPage } from './manage-field-second';

@NgModule({
  declarations: [
    ManageFieldSecondPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageFieldSecondPage),
  ],
})
export class ManageFieldSecondPageModule {}
