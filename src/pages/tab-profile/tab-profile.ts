import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { AuthService } from '../../service/AuthService';
import { ChangePasswordPage } from '../change-password/change-password';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { WithdrawPage } from '../withdraw/withdraw';
import { WebService } from '../../service/WebService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { TermsOfServicePage } from '../terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';
import { SignInPage } from '../sign-in/sign-in';
import firebase from 'firebase';
import { BoopayHistoryPage } from '../boopay-history/boopay-history';

/**
 * Generated class for the TabProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-profile',
  templateUrl: 'tab-profile.html',
})
export class TabProfilePage {

  userProfile:any;
  loading:Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private AuthService: AuthService, private modalCtrl:ModalController,
    private webService: WebService, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  openModalWithdraw() {
    console.log(this.userProfile.balance)
    let modal = this.modalCtrl.create(WithdrawPage,{'balance' : this.userProfile.balance});
    modal.present();
  }
  openEditProfilePage(first_name, last_name, phone){
    this.navCtrl.push(EditProfilePage,{'first_name' : first_name,'last_name' : last_name, 'phone' : phone});
  }
  openChangePasswordPage(){
    this.navCtrl.push(ChangePasswordPage);
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }
  ionViewDidLoad() {
    //this.showLoading();
    this.getProfile();
    console.log('ionViewDidLoad TabProfilePage');
  }

  getProfile(){
    this.presentLoadingDefault();
    let email = firebase.auth().currentUser.email;
    
    let req = {
      'email' : email
    }

    this.webService.post("http://delthraze.esy.es/Boopang/API/get_data_user.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        this.userProfile = responseData;
        console.log(this.userProfile)
      }
    }, error =>{
    })

    console.log(this.userProfile)
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });
    this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);
  }

  logout(){
    let alert = this.alertCtrl.create({
      subTitle: 'Are you sure want to log out',
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            if(this.AuthService.logout()){
              this.navCtrl.push(SignInPage)
            }

          }
        }
      ]
    });
    alert.present();

   }
   openTermsOfServicePage(){
     this.navCtrl.push(TermsOfServicePage);
   }
   openPrivacyPolicyPage(){
      this.navCtrl.push(PrivacyPolicyPage);
   }
   openBoopayHistoryPage(){
     this.navCtrl.push(BoopayHistoryPage);
   }

}

