import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabBookingDonePage } from './tab-booking-done';

@NgModule({
  declarations: [
    TabBookingDonePage,
  ],
  imports: [
    IonicPageModule.forChild(TabBookingDonePage),
  ],
})
export class TabBookingDonePageModule {}
