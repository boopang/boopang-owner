import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { WebService } from '../../service/WebService';
import { TabFieldPage } from '../tab-field/tab-field';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the RegisterFieldThirdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-field-third',
  templateUrl: 'register-field-third.html',
})
export class RegisterFieldThirdPage {
  allForm = this.navParams.data;

  imageUrl = '';
  base64Image:String;
  imageToUpload:String;

  ThirdFieldForm: FormGroup;
  fileValid = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,  
    private camera: Camera, private alertCtrl: AlertController,
     private builder: FormBuilder, private webService: WebService,
    private loadingCtrl:LoadingController) {
  }

  ionViewDidLoad() {
    console.log(this.navParams.data)
    console.log('ionViewDidLoad RegisterFieldThirdPage');
  }

  openGallery(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:1000,
      targetHeight:1000,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }).then((imageData)=>{
      this.base64Image = 'data:image/jpeg;base64,'+imageData;
      this.imageToUpload = imageData;
    },(err)=>{
      console.log(err);
    })
  }

  onTakePhoto() { 
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
    }

    this.camera.getPicture(options).
      then(          (imageData) => {
          this.imageUrl =  imageData;
          
          console.log(this.imageUrl);
        }, 
        (err) => {
          console.log(err);
        });
  }

  
  saveDB(){
    //console.log(this.allForm);
    // let loading = this.loadingCtrl.create({
    //   content: 'Please wait...'
    // });
  
    // loading.present();
    
    // let req = {
    //   "court_detail" : this.allForm.courtDayDetail,
    //   "court_name" : this.allForm.court_name,
    //   "court_type" : this.allForm.court_type,
    //   "court_phone" : this.allForm.court_phone,
    //   "court_city" : this.allForm.court_city,
    //   "court_province" : this.allForm.court_province,
    //   "court_address" : this.allForm.court_street,
    //   "status" : "WAITING APPROVAL",
    //   "image" : this.imageToUpload
    // }

    // this.webService.post("http://delthraze.esy.es/Boopang/API/regis_court.php", JSON.stringify(req), null).subscribe(response => {
    //   let responseData = JSON.parse(response["_body"]);
    //   console.log(responseData['success'])
    //   if(responseData['success']){
    //     this.navCtrl.push(TabFieldPage);
    //     loading.dismiss();
    //   }
    //   else{
    //     loading.dismiss();
    //     let alert = this.alertCtrl.create({
    //       title: 'Save DB Field',
    //       buttons: ['Dismiss']
    //     });
    //     alert.present();
    //   }
    // }, error =>{
    // })

    // let req = {
    //   "image" : this.imageToUpload
    // }

    // this.webService.post("http://delthraze.esy.es/Boopang/API/save_image.php", JSON.stringify(req), null).subscribe(response => {
    //   let responseData = JSON.parse(response["_body"]);
    //   console.log(responseData['success'])
    // }, error =>{
    // })

    this.navCtrl.push(TabsPage)
    
    
  }

}
