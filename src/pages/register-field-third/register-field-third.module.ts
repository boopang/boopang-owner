import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFieldThirdPage } from './register-field-third';

@NgModule({
  declarations: [
    RegisterFieldThirdPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFieldThirdPage),
  ],
})
export class RegisterFieldThirdPageModule {}
