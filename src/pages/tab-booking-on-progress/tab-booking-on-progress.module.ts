import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabBookingOnProgressPage } from './tab-booking-on-progress';

@NgModule({
  declarations: [
    TabBookingOnProgressPage,
  ],
  imports: [
    IonicPageModule.forChild(TabBookingOnProgressPage),
  ],
})
export class TabBookingOnProgressPageModule {}
