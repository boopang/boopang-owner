import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TabBookingWaitingPage } from '../tab-booking-waiting/tab-booking-waiting';
import { TabBookingOnProgressPage } from '../tab-booking-on-progress/tab-booking-on-progress';
import { TabBookingDonePage } from '../tab-booking-done/tab-booking-done';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as moment from 'moment';
import { WebService } from '../../service/WebService';
import firebase from 'firebase';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
/**
 * Generated class for the TabBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-booking',
  templateUrl: 'tab-booking.html',
})
export class TabBookingPage {
  tabWaiting=TabBookingWaitingPage;
  tabOnProgress=TabBookingOnProgressPage;
  tabDone=TabBookingDonePage;

  bookingWACollection:any;
  bookingOPCollection:any;
  bookingDONECollection:any;

  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();
 
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  userInfo: any;

  booking: string;
  calendarOn: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private modalCtrl:ModalController, private alertCtrl:AlertController,
    private webService:WebService, private loadingCtrl:LoadingController ) {

    this.booking = "bookingWaiting"
  }

  addEvent() {
    let modal = this.modalCtrl.create('AddSchedulePage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;
 
        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);
 
        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }
  
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 
  onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');
    
    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK']
    })
    alert.present();
  }
 
  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }

  ionViewWillEnter(){
    this.getBooking();
  }
  ionViewDidLoad() {
    this.getUserInfo()
    //console.log(this.navParams.get('id'))
    this.getBooking();
    console.log('ionViewDidLoad TabBookingPage');
  }

  getUserInfo(){
    firebase.auth().onAuthStateChanged(user =>{
      let req = {
        'email' : user.email
      }

      this.webService.post("http://delthraze.esy.es/Boopang/API/get_data_user.php", JSON.stringify(req), null).subscribe(response => {
        let responseData = JSON.parse(response["_body"]);
        if(responseData){
          this.userInfo = responseData[0];
        }
      }, error =>{
      })

    })
  }

  getBooking(){
    let req = "hai"
    // this.presentLoadingDefault();
    this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_booking.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        this.bookingWACollection = responseData[0];
        this.bookingOPCollection = responseData[1];
        this.bookingDONECollection = responseData[2];
      }
    }, error =>{
    })
  }
  showCalendar(){
    if(this.calendarOn == true){
      this.calendarOn = false;
    }
    else{
      this.calendarOn = true;
    }
  }

  denyAlert(id_booking) {
    let id = id_booking
    let alert = this.alertCtrl.create({
      title: 'Deny',
      message: 'Are you sure want to deny this booking?',
      inputs: [
        {
          name: 'reason',
          placeholder: 'Reason'
        }
      ],
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            this.updateDeny(data.reason, id);
          }
        }
      ]
    });
    alert.present();
  }

  acceptAlert(id_booroom) {
    let id = id_booroom
    let alert = this.alertCtrl.create({
      title: 'Accept',
      message: 'Are you sure want to accept this booking?',
      inputs: [
        {
          name: 'message',
          placeholder: 'Additional message'
        }
      ],
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            this.updateAccept(data.message, id)
          }
        }
      ]
    });
    alert.present();
  }

  updateDeny(message, id){
    let req = {
      'message' : message,
      'id_booking' : id,
      'status' : 'deny'
    }

    this.webService.post("http://delthraze.esy.es/Boopang/API/update_status_booking.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        let alert = this.alertCtrl.create({
          title: 'Denied',
          message: 'Booking has been denied',
          buttons: [
            {
              text: 'Yes',
              handler: data => {
                this.getBooking()
              }
            }
          ]
        });
        alert.present();
      }
    }, error =>{
    })
  }

  updateAccept(message, id){
    let req = {
      'message' : message,
      'id_booking' : id,
      'status' : 'accept'
    }

    this.webService.post("http://delthraze.esy.es/Boopang/API/update_status_booking.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        let alert = this.alertCtrl.create({
          title: 'Accepted',
          message: 'Booking has been accepted',
          buttons: [
            {
              text: 'Yes',
              handler: data => {
                this.getBooking()
              }
            }
          ]
        });
        alert.present();
      }
    }, error =>{
    })
  }

}
