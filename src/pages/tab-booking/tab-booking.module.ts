import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabBookingPage } from './tab-booking';

@NgModule({
  declarations: [
    TabBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(TabBookingPage),
  ],
})
export class TabBookingPageModule {}
