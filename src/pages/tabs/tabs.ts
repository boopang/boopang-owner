import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabBookingPage } from '../tab-booking/tab-booking';
import { TabEventPage } from '../tab-event/tab-event';
import { TabFieldPage } from '../tab-field/tab-field';
import { TabProfilePage } from '../tab-profile/tab-profile';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tabBooking=TabBookingPage;
  tabEvent= TabEventPage;
  tabField=TabFieldPage; 
  tabProfile=TabProfilePage;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
