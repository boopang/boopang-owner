import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the WithdrawPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-withdraw',
  templateUrl: 'withdraw.html',
})
export class WithdrawPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl:AlertController) {
  }

  openAlertConfirmation(){
    let alert = this.alertCtrl.create({
      title: 'Withdraw Confirmation',
      subTitle: 'Please enter your password to proceed this request',
      inputs: [
        {
          name: 'password',
          placeholder: 'Password',
          type:"password"
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {

            console.log('Saved clicked');
          }
        }
      ]
    });
    alert.present();
  }
  ionViewDidLoad() {
    console.log(this.navParams.get('balance'));
    console.log('ionViewDidLoad WithdrawPage');
  }

}
