import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BoopayHistoryPage } from './boopay-history';

@NgModule({
  declarations: [
    BoopayHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(BoopayHistoryPage),
  ],
})
export class BoopayHistoryPageModule {}
