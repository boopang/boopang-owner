import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { DatePicker } from '@ionic-native/date-picker';
import { CameraOptions,Camera } from '@ionic-native/camera';
import { WebService } from '../../service/WebService';
import { TabEventPage } from '../tab-event/tab-event';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
/**
 * Generated class for the AddNewEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-event',
  templateUrl: 'add-new-event.html',
})
export class AddNewEventPage{
  NewEventForm:FormGroup;

  courtCollection: any;

  minDate = new Date().toISOString();
  maxDate = new Date().toISOString();

  base64Image:String;
  imageToUpload:String;
  
  constructor(public navCtrl: NavController,
    private builder: FormBuilder, public navParams: NavParams,
    private camera:Camera, private webService: WebService,
    private alertCtrl: AlertController) {
  }


  openGallery(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:1000,
      targetHeight:1000,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }).then((imageData)=>{
      this.base64Image = 'data:image/jpeg;base64,'+imageData;
      this.imageToUpload = imageData;
    },(err)=>{
      console.log(err);
    })
  }
  ngOnInit(){
    this.formCheck()
   
  }
  ionViewDidLoad() {
    this.getCourtName();
    console.log('ionViewDidLoad AddNewEventPage');
  }

  formCheck(){
    this.NewEventForm =this.builder.group({
      court_name: new FormControl(null,Validators.required),
      event_name: new FormControl(null,Validators.required),
      event_discount: new FormControl(null,Validators.required),
      event_start: new FormControl(null,Validators.required),
      event_end: new FormControl(null,Validators.required)
    })
  }

  getCourtName(){
    // let req = "hai";

    // this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_court_name.php", JSON.stringify(req), null).subscribe(response => {
    //   let responseData = JSON.parse(response["_body"]);
    //   console.log(JSON.stringify(responseData))
    //   if(responseData){
    //     this.courtCollection = responseData;
    //   }
    //   console.log(this.courtCollection)
    // }, error =>{
    // })
  }


  onSubmit(){
  //   let req = {
  //     "id_court"      : this.NewEventForm.value.court_name,
  //     "id_owner"      : '20',
  //     "event_name"      : this.NewEventForm.value.event_name,
  //     "event_discount"  : this.NewEventForm.value.event_discount,
  //     "event_start"     : this.NewEventForm.value.event_start,
  //     "event_end"       : this.NewEventForm.value.event_end,
  //     "image"           : this.imageToUpload
  //   }
  //   console.log(req['id_court'])

  //   this.webService.post("http://delthraze.esy.es/Boopang/API/regis_event.php", JSON.stringify(req), null).subscribe(response => {
  //     let responseData = JSON.parse(response["_body"]);
  //     console.log(responseData)
  //     if(responseData['success']){
  //       this.navCtrl.push(TabEventPage);
  //     }
  //     else{
  //       let alert = this.alertCtrl.create({
  //         title: 'Add Event Failed',
  //         subTitle: 'Add Event ',
  //         buttons: [
  //           {
  //             text: 'Ok',
  //             handler: data => {
  //               this.navCtrl.push(TabEventPage)
  //             }
  //           }
  //         ]
  //       });
  //       alert.present();
  //     }
  //   }, error =>{
  //   })
  }

}
