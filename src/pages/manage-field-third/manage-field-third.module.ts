import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageFieldThirdPage } from './manage-field-third';

@NgModule({
  declarations: [
    ManageFieldThirdPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageFieldThirdPage),
  ],
})
export class ManageFieldThirdPageModule {}
