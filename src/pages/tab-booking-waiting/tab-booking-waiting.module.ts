import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabBookingWaitingPage } from './tab-booking-waiting';

@NgModule({
  declarations: [
    TabBookingWaitingPage,
  ],
  imports: [
    IonicPageModule.forChild(TabBookingWaitingPage),
  ],
})
export class TabBookingWaitingPageModule {}
