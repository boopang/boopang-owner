import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterFieldThirdPage } from '../register-field-third/register-field-third';
import { FormGroup, FormArray } from '@angular/forms/src/model';
import { FormControl, Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the RegisterFieldSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-field-second',
  templateUrl: 'register-field-second.html',
})
export class RegisterFieldSecondPage {
  firstField = this.navParams.data;
  daysCollection = this.firstField['open_days'];

  test = {"tai" : "banget"};

  courtDayDetail: any[] = [];
  courtDayDetailArr: FormArray= null;

  SecondFieldForm: FormGroup;
  registerFieldThirdPage = RegisterFieldThirdPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, private builder: FormBuilder) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterFieldSecondPage');
    console.log(this.firstField)
  }

  ngOnInit(){
    this.SecondFieldForm = this.builder.group({
      courtDayDetail: this.builder.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.builder.group({
      court_day: '',
      start_detail: '',
      end_detail: '',
      price_detail: ''
    });
  }

  addCourtDayDetail(): void{
    this.courtDayDetailArr = this.SecondFieldForm.get('courtDayDetail') as FormArray;
    this.courtDayDetailArr.push(this.createItem());
  }

  onSubmit(){
    var gabungan = this.collect(this.firstField, this.SecondFieldForm.value)
    this.navCtrl.push(RegisterFieldThirdPage, gabungan);
  }

  collect(field1: any, field2: any) {
    var ret = {};
    var len = arguments.length;
    for (var i=0; i<len; i++) {
      for (var p in arguments[i]) {
        if (arguments[i].hasOwnProperty(p)) {
          ret[p] = arguments[i][p];
        }
      }
    }
    return ret;
  }
  
}
