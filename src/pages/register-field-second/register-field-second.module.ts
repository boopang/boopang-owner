import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFieldSecondPage } from './register-field-second';

@NgModule({
  declarations: [
    RegisterFieldSecondPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFieldSecondPage),
  ],
})
export class RegisterFieldSecondPageModule {}
