import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { AddNewEventPage } from '../add-new-event/add-new-event';
import { WebService } from '../../service/WebService';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { updateDate } from 'ionic-angular/util/datetime-util';
import { AuthService } from '../../service/AuthService';
import firebase from 'firebase';

/**
 * Generated class for the TabEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab-event',
  templateUrl: 'tab-event.html',
})
export class TabEventPage {
  eventCollection:any;
  loading: Loading

  updateStatus: string;

  noPromo:boolean = false;
  errorHandling:boolean = false;

  userInfo:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private webService: WebService, private loadingCtrl: LoadingController,
            private alertCtrl: AlertController, private authService: AuthService) {
  }
  
  openAddEventPage(){
    this.navCtrl.push(AddNewEventPage);
  }
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }
  

  ionViewWillEnter(){
    //this.showLoading();
    //console.log(this.authService.id)
    this.getEvent();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabEventPage');
  }

  getProfile(){
      let email = firebase.auth().currentUser.email;
      
      let req = {
        'email' : email
      }
  
      this.webService.post("http://delthraze.esy.es/Boopang/API/get_data_user.php", JSON.stringify(req), null).subscribe(response => {
        let responseData = JSON.parse(response["_body"]);
        console.log(JSON.stringify(responseData))
        if(responseData){
          this.userInfo = responseData;
        }
      }, error =>{
      })
    }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  openAlertDelete(){
    let alert = this.alertCtrl.create({
      title: 'Delete Field',
      subTitle: 'Please Contact Admin If You Want to Delete Your Event',
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });
    alert.present();
  }

  changeStatus(id,status){
    console.log(id)
    console.log(status)

    if(status == "Publish"){
      this.updateStatus = "Unpublish"
    }
    else this.updateStatus = "Publish"

    console.log(this.updateStatus)

    let req = {
      'id_event'  : id,
      'status'    : this.updateStatus
    }

    this.webService.post("http://delthraze.esy.es/Boopang/API/update_status_event.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(responseData)
      if(responseData){
        document.getElementById('status-'+id).innerHTML = this.updateStatus
      }
    }, error =>{
    })

  }

  ngForLast(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }
  }

  getEvent(){
    // let req = {
    //   "id_owner" : this.user
    // }
    let req = "hai"
    // this.presentLoadingDefault();
    this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_event.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        this.eventCollection = responseData;
      }else{
        this.noPromo = true;
      }
    }, error =>{
        this.errorHandling = true;
    }) 
  }

  deleteAlert(){
    let alert = this.alertCtrl.create({
      title: 'Delete',
      message: 'Are you sure want to delete this promo?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            // if (User.isValid(data.username, data.password)) {
            //  } else {
            //   return false;
            // }
          }
        }
      ]
    });
    alert.present();
  }

}
