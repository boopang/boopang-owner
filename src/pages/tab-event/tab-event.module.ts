import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabEventPage } from './tab-event';

@NgModule({
  declarations: [
    TabEventPage,
  ],
  imports: [
    IonicPageModule.forChild(TabEventPage),
  ],
})
export class TabEventPageModule {}
