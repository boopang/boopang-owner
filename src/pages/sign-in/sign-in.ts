import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../service/AuthService';
import firebase from 'firebase';
import { SignUpPage } from '../sign-up/sign-up';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {
  tabsPage = TabsPage;
  SignInForm: FormGroup;


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private authService: AuthService, private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController,
    private app:App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  ngOnInit(){
    this.initializeForm()
  }

  openSignUpPage(){
    this.navCtrl.push(SignUpPage);
  }

  initializeForm(){
    this.SignInForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  onSubmit(){
  // let x = this.authService.login(this.SignInForm.value.email, this.SignInForm.value.password, ()=>{this.app.getRootNav().setRoot(TabsPage); console.log("Kepangil")});   
  // let x = this.authService.signin(this.SignInForm.value.email, this.SignInForm.value.password);   
  this.navCtrl.push(TabsPage);
  
  //  console.log(x);
  }

}
