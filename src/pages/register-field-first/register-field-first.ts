import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { RegisterFieldSecondPage } from "../register-field-second/register-field-second";
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormArray,
  FormControl
} from "@angular/forms";
import days from "../../data/days";
import { Geolocation } from "@ionic-native/geolocation";
import { WebService } from "../../service/WebService";
import { Loc } from "../../models/location";
import { LocationService } from "../../service/LocationService";
import { RegisterFieldThirdPage } from "../register-field-third/register-field-third";

/**
 * Generated class for the RegisterFieldFirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-register-field-first",
  templateUrl: "register-field-first.html"
})
export class RegisterFieldFirstPage {
  marker: Loc;

  daysCollection: { id: string; day: string }[];
  provinceCollection = [
    {
      id: "1",
      name: "Banten"
    },
    {
      id: "2",
      name: "Jawa Barat"
    }
  ];
  cityCollection = [
    {
      id: "1",
      id_province: "1",
      name: "Kota Tangerang"
    },
    {
      id: "2",
      id_province: "1",
      name: "Kabupaten Tangerang"
    },
    {
      id: "3",
      id_province: "2",
      name: "Bandung"
    }
  ];
  citySelectByProvince: any;

  FirstFieldForm: FormGroup;
  isSelectAll = true;
  isUnSelectAll = false;

  locationIsSet = false;

  constructor(
    public navCtrl: NavController,
    private geoloc: Geolocation,
    public locationSvc: LocationService,
    public navParams: NavParams,
    private builder: FormBuilder,
    private webService: WebService
  ) {}
  onSetMarker(event: any) {
    this.marker = new Loc(event.coords.lat, event.coords.lng);
    console.log(this.marker.lat, this.marker.lng);
  }

  onLocate() {
    this.geoloc
      .getCurrentPosition()
      .then(currLocation => {
        this.marker = new Loc(
          currLocation.coords.latitude,
          currLocation.coords.longitude
        );
        this.locationIsSet = true;
        console.log(this.marker.lat);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ionViewDidLoad() {
    this.onLocate();
    // this.getProvince();
    console.log("ionViewDidLoad RegisterFieldFirstPage");
  }

  ngOnInit() {
    this.onLocate();
    this.daysCollection = days;
    this.formCheck();
    this.onChange("all", true);
  }

  formCheck() {
    this.FirstFieldForm = this.builder.group({
      court_name: new FormControl(null, Validators.required),
      court_type: new FormControl(null, Validators.required),
      court_street: new FormControl(null, Validators.required),
      court_province: new FormControl(null, Validators.required),
      court_city: new FormControl(null, Validators.required),
      court_phone: new FormControl(null, Validators.required),
      weekday_price: new FormControl(null),
      weekend_price: new FormControl(null),
      open_days: this.builder.array([])
    });
  }

  onSubmit() {
    this.navCtrl.push(RegisterFieldThirdPage, this.FirstFieldForm.value);
  }

  getProvince() {
    let req = "hai";

    this.webService
      .post(
        "http://delthraze.esy.es/Boopang/API/get_all_province.php",
        JSON.stringify(req),
        null
      )
      .subscribe(
        response => {
          let responseData = JSON.parse(response["_body"]);
          //console.log(responseData)
          if (responseData) {
            this.provinceCollection = responseData;
          }
        },
        error => {}
      );
  }

  onChangeProvince(province) {
    this.getCity(province);
  }

  getCity(id_province) {
    this.citySelectByProvince = [];
    // let req = {
    //   id_province: id_province
    // };

    // this.webService
    //   .post(
    //     "http://delthraze.esy.es/Boopang/API/get_all_city.php",
    //     JSON.stringify(req),
    //     null
    //   )
    //   .subscribe(
    //     response => {
    //       let responseData = JSON.parse(response["_body"]);
    //       console.log(responseData);
    //       if (responseData) {
    //         this.cityCollection = responseData;
    //       }
    //     },
    //     error => {}
    //   );

    for (let index = 0; index < this.cityCollection.length; index++) {
      if(id_province == this.cityCollection[index].id_province){
        this.citySelectByProvince.push(this.cityCollection[index]);
      }
    }
  }

  selectAll() {
    if (this.isSelectAll && !this.isUnSelectAll) {
      this.isSelectAll = false;
      this.isUnSelectAll = true;
      return false;
    } else {
      this.isSelectAll = true;
      this.isUnSelectAll = false;
      // this.isSelect();
      // this.isUnSelect();
    }
  }

  isSelect() {
    if (this.isSelectAll) {
      return true;
    }
    return false;
  }

  unSelectAll() {
    if (!this.isUnSelectAll && this.isSelectAll) {
      this.isUnSelectAll = true;
    }
  }

  unSelect() {
    return !this.isUnSelectAll;
  }

  onChange(day: string, checked) {
    const answers = <FormArray>this.FirstFieldForm.controls.open_days;

    if (checked) {
      if (day == "all") {
        for (var i = 1; i < this.daysCollection.length; i++) {
          let idx = answers.controls.findIndex(
            x => x.value == this.daysCollection[i].day
          );
          if (idx >= 0) {
            continue;
          }

          answers.push(new FormControl(this.daysCollection[i].day));
        }
      } else {
        answers.push(new FormControl(day));
      }
    } else {
      if (day == "all" && !this.isSelectAll) {
        for (var i = 1; i < this.daysCollection.length; i++) {
          let idx = answers.controls.findIndex(
            x => x.value == this.daysCollection[i].day
          );
          if (idx > -1) {
            answers.removeAt(idx);
          }
        }
      } else {
        let idx = answers.controls.findIndex(x => x.value == day);
        if (idx > -1) {
          answers.removeAt(idx);
        }
      }
    }
  }
}
