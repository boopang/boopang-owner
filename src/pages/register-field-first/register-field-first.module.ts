import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFieldFirstPage } from './register-field-first';

@NgModule({
  declarations: [
    RegisterFieldFirstPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFieldFirstPage),
  ],
})
export class RegisterFieldFirstPageModule {}
