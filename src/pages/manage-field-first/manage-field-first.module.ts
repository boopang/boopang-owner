import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageFieldFirstPage } from './manage-field-first';

@NgModule({
  declarations: [
    ManageFieldFirstPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageFieldFirstPage),
  ],
})
export class ManageFieldFirstPageModule {}
