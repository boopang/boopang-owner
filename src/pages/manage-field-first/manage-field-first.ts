import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ManageFieldSecondPage } from '../manage-field-second/manage-field-second';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { WebService } from '../../service/WebService';
import { ManageFieldThirdPage } from '../manage-field-third/manage-field-third';

/**
 * Generated class for the ManageFieldFirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manage-field-first',
  templateUrl: 'manage-field-first.html',
})
export class ManageFieldFirstPage {

  courtDetail= this.navParams.data;
  provinceCollection = [
    {
      "id" : "1",
      "name" : "Banten"
    },
    {
      "id" : "2",
      "name" : "Jawa Barat"
    }
  ]
  cityCollection = [
    {
      "id" : "1",
      "id_province" : "1",
      "name" : "Kota Tangerang"
    },
    {
      "id" : "2",
      "id_province" : "1",
      "name" : "Kabupaten Tangerang"
    },
    {
      "id" : "3",
      "id_province" : "2",
      "name" : "Bandung"
    }
  ]
  citySelectByProvince: any;
  FirstFieldForm: FormGroup;

  isSelectAll = false;
  isUnSelectAll = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private builder: FormBuilder, private webService: WebService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterFieldFirstPage');
  }

  ngOnInit(){
    this.formCheck()
  }

  formCheck(){
    this.FirstFieldForm =this.builder.group({
      court_name:  new FormControl(null,Validators.required),
      court_type:  new FormControl(null,Validators.required),
      court_street:  new FormControl(null,Validators.required),
      court_province: new FormControl(null,Validators.required),
      court_city: new FormControl(null,Validators.required),
      court_phone: new FormControl(null,Validators.required),
      open_days: this.builder.array([])
    })
  }

  onSubmit(){
    console.log(this.FirstFieldForm)
    this.navCtrl.push(ManageFieldThirdPage, this.FirstFieldForm);
  }

  cekprovince(id_province){
    if(id_province == this.courtDetail.court_province){
      return true;
    }
    else{
      return false;
    }
  }

  cekcity(id_city){
    if(id_city == this.courtDetail.court_city){
      return true;
    }
    else{
      return false;
    }
  }

  // getProvince(){
  //   let req = "hai"
    
  //   this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_province.php", JSON.stringify(req), null).subscribe(response => {
  //     let responseData = JSON.parse(response["_body"]);
  //     //console.log(responseData)
  //     if(responseData){
  //       this.provinceCollection = responseData;
  //     }
  //   }, error =>{
  //   })        
  // }

  onChangeProvince(province){
    this.getCity(province);
  }

  getCity(id_province){
    this.citySelectByProvince = [];
    // let req = {
    //   "id_province" : id_province,
    // }
    
    // this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_city.php", JSON.stringify(req), null).subscribe(response => {
    //   let responseData = JSON.parse(response["_body"]);
    //   console.log(responseData)
    //   if(responseData){
    //     this.cityCollection = responseData;
    //   }
    // }, error =>{
    // })
    for (let index = 0; index < this.cityCollection.length; index++) {
      if(id_province == this.cityCollection[index].id_province){
        this.citySelectByProvince.push(this.cityCollection[index]);
      }
    }
  }

  selectAll(){
    if(this.isSelectAll){
      this.isSelectAll = false;
      //this.isSelect();
    }
    else this.isSelectAll = true;
  }

  isSelect(){
    if(this.isSelectAll){
      return true;
    }
    else return false;
  }

  unSelect(){
    return false;
  }

  unSelectAll(){
    this.unSelect();
  }

  onChange(day: string, checked){
    const answers = <FormArray>this.FirstFieldForm.controls.open_days
    
    if(checked) {
      answers.push(new FormControl(day))
    } else {
      let idx = answers.controls.findIndex(x => x.value == day)
      answers.removeAt(idx)
    }
  }

  cektype(court_type){
    if(court_type == this.courtDetail.court_type){
      return true;
    }
    else return false;
  }

}
